# Deployment for HU AdSD internal apps

## Manual procedure for deployment

1. `git clone` the repository
2. `./mvnw package`
3. `docker build -t rubenkooijman/starter -f Dockerfile .`
4. Push the image to the registry using `docker push`
5. Connect to the [OpenICT VPN](https://vpn.open-ict.hu.nl/) using the Sophos client
6. Login to the server with `ssh -l <username> 145.89.192.60`
7. Create a docker network `docker network create testnetwork`
8. Run mysql container `docker run --network testnetwork --name mysql-test --restart=always -e "MYSQL_RANDOM_ROOT_PASSWORD=true" -e "MYSQL_DATABASE=test" -e "MYSQL_USER=testuser" -e "MYSQL_PASSWORD=test1234" mysql`
9. Run the app `docker run --network testnetwork -p 9872:8080 -e "DB_JDBC_URL=jdbc:mysql://mysql-test:3306/test" -e DB_USERNAME=testuser -e DB_PASSWORD=test1234 -ti rubenkooijman/starter:latest`

### Notes
* (Step 2) The tests currently need an installed firefox, maybe use a headless firefox for being able to test on the ci-server.
* (Step 3) [Docker Hub](https://hub.docker.com) is used as a docker registry to host the image, current account is rubenkooijman, use `docker login` to be able to push the image to the registry.
* We could create a Dockerfile that combines steps 1 through 3
* Check the arguments to the docker run commands to suit your needs
* Some of the environment variables (`-e`) could also be provided by [docker secrets](https://docs.docker.com/engine/swarm/secrets/)
* Steps 6, 7 and 8 are maybe eligible to consolidate into a docker-compose file
* For deployment / container orchestration a tech such as docker-swarm could be used
* If we want to host multiple apps on the same server we might need a proxy such as HA-proxy or nginx to control forwarding http traffic

## OpenICT VPN

The virtual server is running within the VPN environment provided by OpenICT Guild CSC (Cyber Security and Cloud). 
Our teacher contact there is 
* Martijn Bellaard. 

Student contacts are 
* Asmir Hanic and 
* Thomas Boots 

To obtain a connection to the VPN:
1. Ask for access to the VPN from Asmir
2. Point your browser to [OpenICT VPN](https://vpn.open-ict.hu.nl/)
3. Download the Sophos client and VPN configuration from this page
4. Install the client and import the downloaded configuration settings
5. Connect to the VPN using the Sophos client
6. The server is now reachable at IP `145.89.192.60`
7. Login to the machine using SSH 

## Security

A few notes on security. 
* The server should only be reachable from within the VPN _unless_ a specific firewall rule is added to allow access to a tcp-port. Contact Asmir to open up one of the ports.
* TLS / HTTPS should be handled by the application, but is currently not implemented. Please make sure this is working correctly before opening up the application to the public.
* Do not publish the database port directly and keep it within the docker network. (e.g. don't use `-P` and only use `-p` in docker to publish the port you really want to be reachable from outside)
* Do not expose passwords in plain text, use a secure mechanism to configure these. (e.g. docker secrets or by using a key vault)
* Before the application is made public (on any change) please involve a second pair of eyes to verify the site is secure.
* Make sure the OS and system libraries of the server are kept up-to-date
* Make sure the dependencies of the app are kept up-to-date in the pom.xml
* Make sure the docker base image (linux+jre) is kept up-to-date