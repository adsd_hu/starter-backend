FROM eclipse-temurin:21-alpine
ADD target/starter-backend-0.0.1-SNAPSHOT.jar starter-backend-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "starter-backend-0.0.1-SNAPSHOT.jar"]
